define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group'], function(d3, widgets, aeon, QueryGroup) {

    const datasetPriorityOrder = ["GSOM"];

    const INTRO_TEXT = '<p>Displays historical weather data from individual weather stations worldwide,'
          + ' provided by the'
          + ' <a href="https://www.ncei.noaa.gov/" target="_blank">National Centers for Environmental Information</a>'
          + ', part of the U.S. Department of Commerce.</p>';

    return function(obj) {
        obj.added = false;
        obj.type = "num";
        obj.ds = new aeon.NOAACDODataset();

        var currentStation, currentDataType, currentDataset;
        var stationInfo = {};

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);
        var form = obj.box.append("form");

        var updateStations = function(location) {
            stationGroup.query(obj.ds.getStations(location, datasetPriorityOrder), function(resp) {
                if (resp.length > 0) {
                    for (let station of resp.sort((a, b) => a.name.localeCompare(b.name))) {
                        stationInfo[station.id] = {
                            "startYear": station.mindate.split("-")[0],
                            "endYear": station.maxdate.split("-")[0],
                        };
                        stationSelect.append("option")
                            .attr("value", station.id)
                            .html(station.name);
                    }
                    stationSelect.attr("disabled", null);
                    stationSelect.dispatch("change");
                } else {
                    stationSelect.append("option")
                        .html("No stations with supported datasets found");
                    stationSelect.attr("disabled", "");
                }
                stationLabel.style("display", "inline");
                stationSelect.style("display", "inline");
            });
        };

        form.append("label").html("Country:");
        var countrySelect = form.append("select")
            .on("change", function() {
                var country = d3.event.target.value;
                if (country == "FIPS:US") {
                    stateLabel.style("visibility", "visible");
                    stateGroup.query(obj.ds.getLocations("ST"), function(resp) {
                        stateLabel.style("display", "inline");
                        stateSelect.style("display", "inline");
                        for (let state of resp.sort((a, b) => a.name.localeCompare(b.name))) {
                            stateSelect.append("option")
                                .attr("value", state.id)
                                .html(state.name);
                        }
                        stateSelect.dispatch("change");
                    });
                } else {
                    stateGroup.invalidate();
                    updateStations(country);
                }
            });
        var countryGroup = new QueryGroup();
        countryGroup.invalidatedBy(obj.rootGroup);

        var stateLabel = form.append("label");
        stateLabel.html("U.S. state:");
        var stateSelect = form.append("select")
            .on("change", function() {
                updateStations(d3.event.target.value);
            });
        var stateGroup = new QueryGroup(function() {
            stateSelect.selectAll("option").remove();
            stateLabel.style("display", "none");
            stateSelect.style("display", "none");
        });
        stateGroup.invalidatedBy(countryGroup);

        var stationLabel = form.append("label").html("Station:");
        var stationSelect = form.append("select")
            .on("change", function() {
                currentStation = d3.event.target.value;
                dataTypeGroup.query(obj.ds.getDataTypes(currentStation, datasetPriorityOrder), function(resp) {
                    if (resp.length > 0) {
                        for (let dataType of resp.sort((a, b) => a.name.localeCompare(b.name))) {
                            dataTypeSelect.append("option")
                                .attr("value", dataType.id)
                                .html(dataType.name);
                        }
                        dataTypeSelect.attr("disabled", null);
                        dataTypeSelect.dispatch("change");
                    } else {
                        dataTypeSelect.append("option")
                            .html("No supported data types found");
                        dataTypeSelect.attr("disabled", "");
                    }
                    dataTypeLabel.style("display", "inline");
                    dataTypeSelect.style("display", "inline");
                });
            });
        var stationGroup = new QueryGroup(function() {
            stationLabel.style("display", "none");
            stationSelect.selectAll("option").remove();
            stationSelect.style("display", "none");
        });
        stationGroup.invalidatedBy(stateGroup);

        var dataTypeLabel = form.append("label").html("Data type:");
        var dataTypeSelect = form.append("select")
            .on("change", function() {
                currentDataType = d3.event.target.value;
                datasetGroup.query(obj.ds.getDatasets(currentStation, currentDataType), function(resp) {
                    var rds = null;
                    for (let ds of datasetPriorityOrder) {
                        var r = resp.find(r => (r.id == ds));
                        if (r !== undefined) {
                            rds = r;
                            break;
                        }
                    }
                    if (rds) {
                        currentDataset = rds.id;
                        obj.ready();
                    } else {
                        console.error("No supported dataset found");
                    }
                });
            });
        var dataTypeGroup = new QueryGroup(function() {
            dataTypeLabel.style("display", "none");
            dataTypeSelect.selectAll("option").remove();
            dataTypeSelect.style("display", "none");
        });
        dataTypeGroup.invalidatedBy(stationGroup);

        var datasetGroup = new QueryGroup(function() { obj.unready(); });
        datasetGroup.invalidatedBy(dataTypeGroup);

        countryGroup.query(obj.ds.getLocations("CNTRY"), function(resp) {
            for (let country of resp.sort((a, b) => a.name.localeCompare(b.name))) {
                var opt = countrySelect.append("option")
                    .attr("value", country.id)
                    .html(country.name);
            }
            countrySelect.dispatch("change");
        });

        obj.createFooterRow(function(obj) {
            obj.ds.setDimensions(currentStation, currentDataset, currentDataType,
                                 stationInfo[currentStation].startYear, stationInfo[currentStation].endYear);
        });
    };
});
