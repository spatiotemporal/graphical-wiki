define(function() {

    // This class represents a group of queries where only the
    // response to the most recently sent query is to be
    // considered. Thus, once queries #3 and #4 have both been sent,
    // the response to query #3 should be discarded, regardless of in
    // which order the responses arrive.

    // The optional invalidateCallback will be called whenever this
    // query is invalidated, either explicitly from a query() or
    // invalidate() call, or due to a dependency.
    function QueryGroup(invalidateCallback = null) {
        var self = this;
        this.queryNr = 0;
        this.dependants = [];

        this.update = function() {
            if (invalidateCallback !== null) {
                invalidateCallback();
            }
            for (let dep of self.dependants) {
                dep.invalidate();
            }
            return ++self.queryNr;
        };

        if (invalidateCallback !== null) {
            invalidateCallback();
        }
    };

    // Register a new query whose response must already be setup to be
    // delivered by queryPromise. When the response arrives
    // (i.e. queryPromise is resolved), it is fed to the callback
    // function if and only if it belongs to the most recently made
    // query.
    QueryGroup.prototype.query = function(queryPromise, callback) {
        var self = this;
        var qn = self.update();
        queryPromise.then(function(resp) {
            if (qn == self.queryNr) {
                callback(resp);
            } else {
                console.log("Ignoring query %d, awaiting query %d", qn, self.queryNr);
            }
        });
    };

    // Mark the response to the last query to be ignored, just as if
    // yet another query had just been made.
    QueryGroup.prototype.invalidate = function() {
        this.update();
    };

    // Register a dependency - sending a query in this group will also
    // invalidate outstanding depGroup queries.
    QueryGroup.prototype.willInvalidate = function(depGroup) {
        this.dependants.push(depGroup);
    };

    // Register the same type of dependency as willInvalidate(), only
    // the syntax is reversed.
    QueryGroup.prototype.invalidatedBy = function(group) {
        group.willInvalidate(this);
    };

    return QueryGroup;
});
