define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group'], function(d3, widgets, aeon, QueryGroup) {

    const INTRO_TEXT = '<p>Displays numeric datasets from the'
          + ' <a href="https://www.who.int/" target="_blank">World Health Organization</a> available from their'
          + ' <a href="https://apps.who.int/gho/data/node.resources.api" target="_blank">Athena web service</a>.</p>';

    return function(obj) {
        var indicatorTree = {};
        var dimensions = [];
        obj.added = false;
        obj.type = "num";
        obj.ds = new aeon.WHODataset();

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);
        var form = obj.box.append("form");

        form.append("label").html("Indicator:");
        var indCatSelect = form.append("select")
            .on("change", function() {
                var indData = indSelect.selectAll("option").data(indicatorTree[d3.event.target.value]);
                indData.enter().append("option")
                    .merge(indData)
                    .attr("value", d => d.id)
                    .html(d => d.name);
                indData.exit().remove();
                indSelect.dispatch("change");
            });
        form.append("span");
        var indSelect = form.append("select")
            .on("change", function() {
                dimGroup.query(obj.ds.getDimensions(d3.event.target.value), function(resp) {
                    dimensions = resp;
                    for (let dim of dimensions) {
                        dim.label = form.append("label").html(dim.name + ":");
                        dim.sel = form.append("select");
                        dim.sel.selectAll("option").data(dim.values).enter().append("option")
                            .attr("value", d => d.id)
                            .html(d => (d.name));
                    }
                    obj.ready();
                });
            });

        var infoArea = obj.box.append("div").attr("class", "dataset-info").style("visibility", "hidden");

        var dimGroup = new QueryGroup(function() {
            obj.unready();
            for (let dim of dimensions) {
                dim.label.remove();
                dim.sel.remove();
            }
            infoArea.selectAll("*").remove();
            infoArea.style("visibility", "hidden");
        });
        dimGroup.invalidatedBy(obj.rootGroup);

        obj.ds.getIndicators().then(function(resp) {
            indicatorTree = resp;
            for (let [catName, inds] of Object.entries(resp).sort(function(a, b) {
                if (a[0] == "(uncategorized)") return +1;
                if (b[0] == "(uncategorized)") return -1;
                if (a[0] < b[0]) return -1;
                if (a[0] > b[0]) return +1;
                return 0;
            })) {
                var opt = indCatSelect.append("option")
                    .attr("value", catName)
                    .html(catName);
            }
            indCatSelect.dispatch("change");
        });

        obj.createFooterRow(function(obj) {
            var dims = Object.fromEntries(dimensions.map(function(dim) {
                return [dim.id, dim.sel.property("value")];
            }));
            dims.indicator = indSelect.property("value");
            obj.ds.setDimensions(dims);
        }, function(res) {
            var infoAreaUsed = false;
            infoArea.selectAll("*").remove();
            if ("description" in res) {
                infoArea.append("label").html("Description:");
                infoArea.append("span").html(res.description);
                infoAreaUsed = true;
            }
            if ("links" in res) {
                infoArea.append("label").html("Links:");
                var linkSpan = infoArea.append("span");
                var first = true;
                for (let link of res.links) {
                    if (first) {
                        first = false;
                    } else {
                        linkSpan.append("br");
                    }
                    linkSpan.append("a").attr("href", link.url).attr("target", "_blank").html(link.title);
                }
                infoAreaUsed = true;
            }
            if (res.data.length == 0) {
                infoArea.append("label").html("Warning:");
                infoArea.append("span").html("Dataset contains no data");
                infoAreaUsed = true;
            } else if (res.data.length == 1) {
                infoArea.append("label").html("Warning:");
                infoArea.append("span").html("Dataset only contains one single data point");
                infoAreaUsed = true;
            }
            infoArea.style("visibility", infoAreaUsed ? "visible" : "hidden");
        });
    };
});
