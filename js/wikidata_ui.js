define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group',
        'js/wikidata_person_ui', 'js/wikidata_event_ui', 'js/wikidata_work_ui'],
       function(d3, widgets, aeon, QueryGroup, personUI, eventUI, workUI) {

    const INTRO_TEXT = '<p>Displays entities from <a href="https://www.wikidata.org/" target="_blank">Wikidata</a>,'
          + ' a collaboratively edited knowledge base hosted by the'
          + ' <a href="https://wikimediafoundation.org/" target="_blank">Wikimedia Foundation</a>.'

    return function(obj) {
        obj.added = false;
        obj.type = "entity";
        obj.ds = new aeon.WikidataSet();

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);

        var types = {
            "person": {
                label: "Persons",
                create: personUI,
                is_created: false,
            },
            "event": {
                label: "Events",
                create: eventUI,
                is_created: false,
            },
            "work": {
                label: "Creative works",
                create: workUI,
                is_created: false,
            },
        };

        var typeP = obj.box.append("p");
        typeP.append("label").html("Type:");
        var typeSpan = typeP.append("span");
        var typeSelect = new widgets.radioButtonGroup(Object.keys(types).map(function(id) {
            return { id: id, label: types[id].label };
        }));
        typeSelect.on("change", function(id) {
            for (let t of Object.values(types)) {
                t.form.style("display", "none");
            }
            current_type = types[id];
            if (!current_type.is_created) {
                current_type.create(obj, current_type);
                current_type.is_created = true;
            }
            current_type.form.style("display", "grid");
        });
        typeP.append(typeSelect.node());
        for (let t of Object.keys(types)) {
            types[t].form = obj.box.append("form");
        }
        typeSelect.dispatch("change");
        t = typeSelect;

        obj.createFooterRow(function(obj) {
            obj.ds.setFilters(current_type.filters.getAll());
            if (current_type == types["person"] && current_type.filters.includes("position")) {
                obj.setEntityStyle("highlight-children");
            } else {
                obj.setEntityStyle("sub-box-interior");
            }
        });

        obj.ready();
    };
});
