define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group', 'js/wikidata_helpers'],
       function(d3, widgets, aeon, QueryGroup, helpers) {

    const GENDERS = [
        { "id": "Q6581097", "name": "Male" },
        { "id": "Q6581072", "name": "Female" },
        { "id": "Q2449503", "name": "Transgender male" },
        { "id": "Q1052281", "name": "Transgender female" },
        { "id": "Q48270",   "name": "Non-binary" },
        { "id": "Q1097630", "name": "Intersex" },
    ];

    const MANNERS_OF_DEATH = [
        { "id": "Q3739104", "name": "Natural causes" },
        { "id": "Q171558", "name": "Accident" },
        { "id": "Q906512", "name": "Shipwrecking" },
        { "id": "Q25047808", "name": "Death by misadventure" },
        { "id": "Q100159", "name": "Euthanasia" },
        { "id": "Q10737", "name": "Suicide" },
        { "id": "Q149086", "name": "Homicide" },
        { "id": "Q8454", "name": "Capital punishment" },
        { "id": "Q2717573", "name": "Extra-judicial killing" },
        { "id": "Q18663901", "name": "Death in battle" },
        { "id": "Q2223653", "name": "Terrorist attack" },
        { "id": "Q191503", "name": "Duel" },
        { "id": "Q15729048", "name": "Pending investigation" },
    ];

    return function(obj, typeData) {
        var form = typeData.form;
        var filters = typeData.filters = new helpers.FilterSet();
        filters.set("instance-of", "Q5");

        var occupationSelect = new widgets.searchBar();
        obj.ds.getOccupations().then(function(resp) {
            occupationSelect.setEntries(resp);
            occupationSelect.setFrozenValueMode(true);
            occupationSelect.setSubmitByEntryMode(true);
            occupationSelect.on("submit", function(e) {
                filters.set("occupation", e);
            });
        });
        form.append("label").html("Occupation:");
        form.append(occupationSelect.node());

        form.append("label").html("Country of citizenship:");
        helpers.countryControl(form, obj.ds, filters, "citizenship");

        form.append("label").html("Place of birth:");
        helpers.placeControl(form, obj.rootGroup, obj.ds, filters, "country-of-birth", "region-of-birth");

        form.append("label").html("Place of death:");
        helpers.placeControl(form, obj.rootGroup, obj.ds, filters, "country-of-death", "region-of-death");

        form.append("label").html("Place of burial:");
        helpers.placeControl(form, obj.rootGroup, obj.ds, filters, "country-of-burial", "region-of-burial");

        form.append("label").html("Manner of death:");
        var mannerOfDeathSelect = form.append("select")
            .on("change", function() {
                var manner = d3.event.target.value;
                filters.set("manner-of-death", (manner === "any") ? null : manner);
            });
        mannerOfDeathSelect.append("option")
            .attr("value", "any")
            .html("(any manner)");
        for (let manner of MANNERS_OF_DEATH) {
            mannerOfDeathSelect.append("option")
                .attr("value", manner.id)
                .html(manner.name);
        }

        form.append("label").html("Gender:");
        var genderSelect = form.append("select")
            .on("change", function() {
                var gender = d3.event.target.value;
                filters.set("gender", (gender === "any") ? null : gender);
            });
        genderSelect.append("option")
            .attr("value", "any")
            .html("(any gender)");
        for (let gender of GENDERS) {
            genderSelect.append("option")
                .attr("value", gender.id)
                .html(gender.name);
        }

        var nativeLanguageSelect = new widgets.searchBar();
        obj.ds.getLanguages().then(function(resp) {
            nativeLanguageSelect.setEntries(resp);
            nativeLanguageSelect.setFrozenValueMode(true);
            nativeLanguageSelect.setSubmitByEntryMode(true);
            nativeLanguageSelect.on("submit", function(e) {
                filters.set("native-language", e);
            });
        });
        form.append("label").html("Native language:");
        form.append(nativeLanguageSelect.node())

        var usedLanguageSelect = new widgets.searchBar();
        obj.ds.getLanguages().then(function(resp) {
            usedLanguageSelect.setEntries(resp);
            usedLanguageSelect.setFrozenValueMode(true);
            usedLanguageSelect.setSubmitByEntryMode(true);
            usedLanguageSelect.on("submit", function(e) {
                filters.set("used-language", e);
            });
        });
        form.append("label").html("Used language:");
        form.append(usedLanguageSelect.node())

        var movementSelect = new widgets.searchBar();
        obj.ds.getMovements().then(function(resp) {
            movementSelect.setEntries(resp);
            movementSelect.setFrozenValueMode(true);
            movementSelect.setSubmitByEntryMode(true);
            movementSelect.on("submit", function(e) {
                filters.set("movement", e);
            });
        });
        form.append("label").html("Movement:");
        form.append(movementSelect.node())

        var religionSelect = new widgets.searchBar();
        obj.ds.getReligions().then(function(resp) {
            religionSelect.setEntries(resp);
            religionSelect.setFrozenValueMode(true);
            religionSelect.setSubmitByEntryMode(true);
            religionSelect.on("submit", function(e) {
                filters.set("religion", e);
            });
        });
        form.append("label").html("Religion:");
        form.append(religionSelect.node())

        var politicalPartySelect = new widgets.searchBar();
        obj.ds.getPoliticalParties().then(function(resp) {
            politicalPartySelect.setEntries(resp);
            politicalPartySelect.setFrozenValueMode(true);
            politicalPartySelect.setSubmitByEntryMode(true);
            politicalPartySelect.on("submit", function(e) {
                filters.set("political_party", e);
            });
        });
        form.append("label").html("Member of political party:");
        form.append(politicalPartySelect.node())

        var musicalEnsembleSelect = new widgets.searchBar();
        obj.ds.getMusicalEnsembles().then(function(resp) {
            musicalEnsembleSelect.setEntries(resp);
            musicalEnsembleSelect.setFrozenValueMode(true);
            musicalEnsembleSelect.setSubmitByEntryMode(true);
            musicalEnsembleSelect.on("submit", function(e) {
                filters.set("musical_ensemble", e);
            });
        });
        form.append("label").html("Member of musical ensemble:");
        form.append(musicalEnsembleSelect.node())

        var sportsTeamSelect = new widgets.searchBar();
        obj.ds.getSportTeams().then(function(resp) {
            sportsTeamSelect.setEntries(resp);
            sportsTeamSelect.setFrozenValueMode(true);
            sportsTeamSelect.setSubmitByEntryMode(true);
            sportsTeamSelect.on("submit", function(e) {
                filters.set("sports_team", e);
            });
        });
        form.append("label").html("Member of sports team:");
        form.append(sportsTeamSelect.node())

        var awardSelect = new widgets.searchBar();
        obj.ds.getAwards().then(function(resp) {
            awardSelect.setEntries(resp);
            awardSelect.setFrozenValueMode(true);
            awardSelect.setSubmitByEntryMode(true);
            awardSelect.on("submit", function(e) {
                filters.set("award", e);
            });
        });
        form.append("label").html("Award received:");
        form.append(awardSelect.node())

        var updatePositionFilter = function() {
            if (selectedPosition !== null) {
                filters.set("position", [selectedPosition, selectedPositionQualifier]);
            } else {
                filters.remove("position");
            }
        };
        var selectedPosition = null;
        var selectedPositionQualifier = null;
        var positionSelect = new widgets.searchBar();
        obj.ds.getPositions().then(function(resp) {
            positionSelect.setEntries(resp);
            positionSelect.setFrozenValueMode(true);
            positionSelect.setSubmitByEntryMode(true);
            positionSelect.on("submit", function(e) {
                if (e !== null) {
                    selectedPosition = e.id;
                    updatePositionFilter();
                    posFlagGroup.query(obj.ds.positionNeedsQualifier(e.id), function(flag) {
                        if (flag) {
                            posQualifierGroup.query(obj.ds.getOrganizationsWithPosition(e.id), function(resp) {
                                positionQualifierSelect.setEntries(
                                    resp.sort((a, b) => (a.name.localeCompare(b.name))));
                                positionQualifierFill.style("display", "inline");
                                positionQualifierSpan.style("display", "grid");
                            });
                        }
                    });
                } else {
                    selectedPosition = null;
                    posFlagGroup.invalidate();
                }
            });
        });
        var positionQualifierSelect = new widgets.searchBar();
        positionQualifierSelect.setFrozenValueMode(true);
        positionQualifierSelect.setSubmitByEntryMode(true);
        positionQualifierSelect.setMinInputCharacters(1);
        positionQualifierSelect.on("submit", function(e) {
            if (e !== null) {
                selectedPositionQualifier = e.id;
            } else {
                selectedPositionQualifier = null;
            }
            updatePositionFilter();
        });
        form.append("label").html("Position held:");
        form.append(positionSelect.node())
        var positionQualifierFill = form.append("span");
        var positionQualifierSpan = form.append("span")
            .style("grid-template-columns", "max-content 1fr")
            .style("column-gap", "10px");
        positionQualifierSpan.append("span").html("of ");
        positionQualifierSpan.append(positionQualifierSelect.node())
        var posFlagGroup = new QueryGroup(function() {
        });
        var posQualifierGroup = new QueryGroup(function() {
            positionQualifierSelect.clear();
            positionQualifierFill.style("display", "none");
            positionQualifierSpan.style("display", "none");
            selectedPositionQualifier = null;
            updatePositionFilter();
        });
        posFlagGroup.invalidatedBy(obj.rootGroup);
        posQualifierGroup.invalidatedBy(posFlagGroup);
    }

});
