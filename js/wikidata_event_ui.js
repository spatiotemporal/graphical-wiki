define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group', 'js/wikidata_helpers'],
       function(d3, widgets, aeon, QueryGroup, helpers) {

    const EVENT_TYPES = [
        { "id": "Q27968043,Q132241", "name": "Festivals" },
        { "id": "Q18608583", "name": "Recurring sporting events" },
        { "id": "Q27020041", "name": "Sports seasons" },
        { "id": "Q1673271", "name": "Regime changes" },
        { "id": "Q198", "name": "Wars" },
        { "id": "Q3839081", "name": "Disasters" },
    ];

    return function(obj, typeData) {
        var form = typeData.form;
        var filters = typeData.filters = new helpers.FilterSet();

        filters.set("sub-event", true);

        form.append("label").html("Type of event:");
        var eventTypeSelect = form.append("select")
            .on("change", function() {
                var types = d3.event.target.value;
                filters.set("instance-of-subclass-of",
                            (types === "any") ? EVENT_TYPES.map(e => e.id.split(",")).flat() : types.split(","));
            });
        eventTypeSelect.append("option")
            .attr("value", "any")
            .html("(any type)");
        for (let type of EVENT_TYPES) {
            eventTypeSelect.append("option")
                .attr("value", type.id)
                .html(type.name);
        }
        eventTypeSelect.dispatch("change");

        form.append("label").html("Country:");
        helpers.countryControl(form, obj.ds, filters, "country");

        form.append("label").html("Number of participants:");
        helpers.rangeControl(form, filters, "number-of-participants");
        form.append("label").html("Number of survivors:");
        helpers.rangeControl(form, filters, "number-of-survivors");
        form.append("label").html("Number of injured:");
        helpers.rangeControl(form, filters, "number-of-injured");
        form.append("label").html("Number of deaths:");
        helpers.rangeControl(form, filters, "number-of-deaths");

        var sportSelect = new widgets.searchBar();
        obj.ds.getSports().then(function(resp) {
            sportSelect.setEntries(resp);
            sportSelect.setFrozenValueMode(true);
            sportSelect.setSubmitByEntryMode(true);
            sportSelect.on("submit", function(e) {
                filters.set("sport", e);
            });
        });
        form.append("label").html("Sport:");
        form.append(sportSelect.node())
    }

});
