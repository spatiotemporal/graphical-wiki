define(['d3', 'aeon/aeon'], function(d3, aeon) {

    const INTRO_TEXT = '<p>Displays aggregated climate data from the'
          + ' <a href="https://www.giss.nasa.gov/" target="_blank">Goddard Institute for Space Studies</a> at NASA.</p>';

    return function(obj) {
        obj.added = false;
        obj.type = "num";
        obj.ds = new aeon.NASAGoddardDataset();

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);
        var form = obj.box.append("form");

        form.append("label").html("Dataset:")
        var sel = form.append("select");
        sel.selectAll("option").data(obj.ds.getSeries()).enter().append("option")
            .attr("value", s => s.id)
            .html(s => s.name);
        sel.on("change", function() {
            obj.ds.setSeries(d3.event.target.value);
        });
        sel.dispatch("change");

        var infoArea = obj.box.append("div").attr("class", "dataset-info").style("visibility", "hidden");

        obj.createFooterRow(function(obj) {
        }, function(res) {
            infoArea.style("visibility", "visible");
            infoArea.selectAll("*").remove();
            infoArea.append("label").html("Description:");
            infoArea.append("span").html(res.description);
            infoArea.append("label").html("References:");
            infoArea.append("span").html(res.references);
        });

        obj.ready();
    };
});
