define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group'], function(d3, widgets, aeon, QueryGroup) {

    const RANGE_CONTROL_FIELD_SIZE = 11;
    const INFINITY_SYMBOL = "\u221E";

    /* FilterSet class represents a set of filters which is updated by
     * the corresponding UI controls. */
    function FilterSet() {
        this.filters = {};
    }

    FilterSet.prototype.set = function(f, e) {
        if (e !== null) {
            if (typeof e == "string" || Array.isArray(e)) {
                this.filters[f] = e;
            } else {
                this.filters[f] = e.id;
            }
        } else {
            delete this.filters[f];
        }
    };

    FilterSet.prototype.remove = function(f) {
        delete this.filters[f];
    };

    FilterSet.prototype.includes = function(f) {
        return f in this.filters;
    };

    FilterSet.prototype.getAll = function() {
        return this.filters;
    };

    var languagesByCountry = {};

    function rangeControl(parent, filterSet, rangeFilterName) {
        var span = parent.append("span")
            .style("display", "grid")
            .style("grid-template-columns", "1fr max-content 1fr")
            .style("column-gap", "5px");
        var low = null, high = null;
        span.append("input")
            .attr("type", "text")
            .attr("size", RANGE_CONTROL_FIELD_SIZE)
            .attr("value", "0")
            .on("input", function() {
                var s = d3.event.target.value;
                if (/^\d*$/.test(s)) {
                    var v = parseInt(s, 10);
                    if (s === "") {
                        low = null;
                        return;
                    } else if (v >= 0) {
                        low = v;
                        return;
                    }
                }
                d3.select(d3.event.target).property("value", low);
            })
            .on("change", function() {
                var s = d3.event.target.value;
                if (/^\d+$/.test(s)) {
                    low = parseInt(s, 10);
                    if (low == 0) {
                        low = null;
                    }
                } else {
                    low = null;
                }
                if (low === null && high === null) {
                    filterSet.remove(rangeFilterName);
                } else {
                    filterSet.set(rangeFilterName, [low, high]);
                }
            })
            .on("blur", function() {
                if (d3.event.target.value === "") {
                    d3.select(d3.event.target).property("value", "0");
                }
            });
        span.append("span").html(" &ndash; ");
        span.append("input")
            .attr("type", "text")
            .attr("size", RANGE_CONTROL_FIELD_SIZE)
            .attr("value", INFINITY_SYMBOL)
            .on("input", function() {
                var s = d3.event.target.value;
                if (/^\d*$/.test(s)) {
                    var v = parseInt(s, 10);
                    if (s === "") {
                        high = null;
                        return;
                    } else if (v >= 0) {
                        high = v;
                        return;
                    }
                }
                d3.select(d3.event.target).property("value", high);
            })
            .on("change", function() {
                var s = d3.event.target.value;
                if (/^\d+$/.test(s)) {
                    high = parseInt(s, 10);
                } else {
                    high = null;
                }
                if (low === null && high === null) {
                    filterSet.remove(rangeFilterName);
                } else {
                    filterSet.set(rangeFilterName, [low, high]);
                }
            })
            .on("blur", function() {
                if (d3.event.target.value === "") {
                    d3.select(d3.event.target).property("value", INFINITY_SYMBOL);
                }
            })
            .on("focus", function() {
                var e = d3.select(d3.event.target);
                if (e.property("value") === INFINITY_SYMBOL) {
                    e.property("value", "")
                }
            });
    };

    function countryControl(parent, wikidataSet, filterSet, countryFilterName) {
        var countrySelect = parent.append("select");
        countrySelect.on("change", function() {
            var country = d3.event.target.value;
            filterSet.set(countryFilterName, (country === "any") ? null : country);
        });
        wikidataSet.getCountries().then(function(resp) {
            countrySelect.append("option")
                .attr("value", "any")
                .html("(any country)");
            for (let country of resp) {
                languagesByCountry[country.id] = country.languages;
                countrySelect.append("option")
                    .attr("value", country.id)
                    .html(country.name);
            }
        });
    };

    function placeControl(parent, rootGroup, wikidataSet, filterSet, countryFilterName, regionFilterName) {
        var selectedCountry = "any";
        var selectedSub1 = "any";
        var selectedSub2 = "any";
        var updateFilters = function() {
            if (selectedCountry === "any") {
                filterSet.remove(countryFilterName);
                filterSet.remove(regionFilterName);
            } else {
                if (selectedSub1 === "any") {
                    filterSet.set(countryFilterName, selectedCountry);
                    filterSet.remove(regionFilterName);
                } else {
                    filterSet.remove(countryFilterName);
                    filterSet.set(regionFilterName, selectedSub2 === "any" ? selectedSub1 : selectedSub2);
                }
            }
        };
        var countrySelect = parent.append("select")
            .on("change", function() {
                selectedCountry = d3.event.target.value;
                if (selectedCountry === "any") {
                    sub1Group.invalidate();
                } else {
                    sub1Group.query(wikidataSet.getFirstLevelCountrySubdivisions(
                        selectedCountry, languagesByCountry[selectedCountry]), function(resp) {
                            sub1Fill.style("display", "inline");
                            sub1Select.style("display", "inline");
                            sub1Select.append("option")
                                .attr("value", "any")
                                .html("(any region)");
                            for (let sub1 of resp.sort((a, b) => a.name.localeCompare(
                                b.name, languagesByCountry[selectedCountry][0]))) {
                                sub1Select.append("option")
                                    .attr("value", sub1.id)
                                    .html(sub1.name);
                            }
                        });
                }
                updateFilters();
            });
        var sub1Fill = parent.append("span");
        var sub1Select = parent.append("select")
            .on("change", function() {
                selectedSub1 = d3.event.target.value;
                if (selectedSub1 === "any") {
                    sub2Group.invalidate();
                } else {
                    sub2Group.query(wikidataSet.getSecondLevelCountrySubdivisions(
                        selectedSub1, languagesByCountry[selectedCountry]), function(resp) {
                            sub2Fill.style("display", "inline");
                            sub2Select.style("display", "inline");
                            sub2Select.append("option")
                                .attr("value", "any")
                                .html("(any region)");
                            for (let sub2 of resp.sort((a, b) => a.name.localeCompare(
                                b.name, languagesByCountry[selectedCountry][0]))) {
                                sub2Select.append("option")
                                    .attr("value", sub2.id)
                                    .html(sub2.name);
                            }
                        });
                }
                updateFilters();
            });
        var sub2Fill = parent.append("span");
        var sub2Select = parent.append("select")
            .on("change", function() {
                selectedSub2 = d3.event.target.value;
                updateFilters();
            });
        var sub1Group = new QueryGroup(function() {
            selectedSub1 = "any";
            sub1Select.selectAll("option").remove();
            sub1Fill.style("display", "none");
            sub1Select.style("display", "none");
        });
        sub1Group.invalidatedBy(rootGroup);
        var sub2Group = new QueryGroup(function() {
            selectedSub2 = "any";
            sub2Select.selectAll("option").remove();
            sub2Fill.style("display", "none");
            sub2Select.style("display", "none");
        });
        sub2Group.invalidatedBy(sub1Group);
        wikidataSet.getCountries().then(function(resp) {
            countrySelect.append("option")
                .attr("value", "any")
                .html("(any country)");
            for (let country of resp) {
                languagesByCountry[country.id] = country.languages;
                countrySelect.append("option")
                    .attr("value", country.id)
                    .html(country.name);
            }
        });
    };

    return {
        FilterSet: FilterSet,
        rangeControl: rangeControl,
        countryControl: countryControl,
        placeControl: placeControl,
    };
});
