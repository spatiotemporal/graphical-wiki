define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/units', 'js/query_group',
        'js/wikidata_ui',
        'js/themoviedb_ui', 'js/discogs_ui', 'js/who_ui', 'js/nasa_goddard_ui', 'js/noaa_cdo_ui'],
       function(d3, widgets, aeon, units, QueryGroup,
                wikidataUI,
                theMovieDBUI, discogsUI, whoUI, nasaGoddardUI, NOAACDOUI) {

    const INITIAL_START_YEAR = 1900;
    const TIME_MARGIN_FRACTION = 0.05;
    const TIME_MARGIN_ABSOLUTE = (365 * 24 * 3600);

    const LOGO_PATH = "images/logos"

    const CALENDARS = [
        {
            id: "none",
            name: "None",
            instance: null,
        },
        {
            id: "unix",
            name: "UNIX timestamps",
            instance: new aeon.UnixEpochCalendar(),
        },
        {
            id: "bp",
            name: "Before Present (1950)",
            instance: new aeon.BeforePresentCalendar(),
        },
        {
            id: "astro",
            name: "Astronomical year calendar",
            instance: new aeon.AstronomicalYearCalendar(),
        },
        {
            id: "egyptian",
            name: "Ancient Egyptian calendar",
            instance: new aeon.AncientEgyptianCalendar(),
        },
        {
            id: "julian",
            name: "Julian calendar",
            instance: new aeon.JulianCalendar(),
        },
        {
            id: "byzantine",
            name: "Byzantine calendar",
            instance: new aeon.ByzantineCalendar(),
        },
        {
            id: "gregorian",
            name: "Gregorian calendar",
            instance: new aeon.GregorianCalendar(),
        },
        {
            id: "gregorian_weekday",
            name: "Gregorian week date (ISO 8601)",
            instance: new aeon.GregorianWeekdayCalendar(),
        },
        {
            id: "hebrew",
            name: "Hebrew calendar",
            instance: new aeon.HebrewCalendar(),
        },
    ];
    const DEFAULT_UPPER_CALENDAR = "none";
    const DEFAULT_LOWER_CALENDAR = "gregorian";

    const ENTITY_COLORS = ["b:#fbb", "b:#bfb", "b:#bbf", "b:#ffb", "b:#fbf", "b:#bff", "b:#bbb",
                           "d:#e00", "d:#0a0", "d:#00e", "d:#e70", "d:#a0a", "d:#0aa", "d:#000"];
    const ENTITY_SECONDARY_COLOR_BRIGHT = "#ddd";
    const ENTITY_SECONDARY_COLOR_DARK = "#777";
    const PLOT_COLORS = ["#f00", "#0b0", "#00f", "#e70", "#f0f", "#0dd", "#999", "#000"];

    const DEFAULT_PLOT_POINT_STYLE = "circle";
    const DEFAULT_PLOT_LINE_STYLE = "dashed";

    const ENTITY_HEIGHTS = [
        {
            name: "Small",
            height: 20,
        },
        {
            name: "Medium",
            height: 40,
        },
        {
            name: "Large",
            height: 60,
        },
    ];
    const DEFAULT_ENTITY_HEIGHT = "Medium";

    const STYLE_ICON_WIDTH = 30;
    const STYLE_ICON_HEIGHT = 18;
    const STYLE_ICON_POINT_SIZE = 14;

    var entityColorStyles = {
        name: "Color",
        entries: ENTITY_COLORS.map(color => ({
            id: color,
            construct: function(elem) {
                elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px")
                    .style("background-color", color.substring(2));
            },
        })),
    };

    var plotColorStyles = {
        name: "Color",
        entries: PLOT_COLORS.map(color => ({
            id: color,
            construct: function(elem) {
                elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px")
                    .style("background-color", color);
            },
        })),
    };

    var plotPointStyles = {
        name: "Point style",
        entries: aeon.Timeline.getPlotPointStyles().map(s => ({
            id: s.name,
            createSymbol: s.createSymbol,
            construct: function(elem) {
                var svg = elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px");
                var g = svg.append("g")
                    .attr("stroke", "black")
                    .attr("transform", "translate("
                          + ((STYLE_ICON_WIDTH - STYLE_ICON_POINT_SIZE) / 2).toString() + ","
                          + ((STYLE_ICON_HEIGHT - STYLE_ICON_POINT_SIZE) / 2).toString() + ")");
                s.createSymbol(g, STYLE_ICON_POINT_SIZE);
            },
        })),
    };

    var plotLineStyles = {
        name: "Line style",
        entries: aeon.Timeline.getPlotLineStyles().map(s => ({
            id: s.name,
            pattern: s.pattern,
            construct: function(elem) {
                var svg = elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px");
                if ("pattern" in s) {
                    svg.append("line")
                        .attr("stroke", "black")
                        .attr("x1", 0)
                        .attr("x2", STYLE_ICON_WIDTH)
                        .attr("y1", STYLE_ICON_HEIGHT / 2)
                        .attr("y2", STYLE_ICON_HEIGHT / 2)
                        .attr("stroke-width", 1)
                        .attr("stroke-dasharray", (s.pattern === null) ? "none" : s.pattern.join(','));
                }
            },
        })),
    };

    const dataSources = [
        {
            fullName: "Wikidata",
            shortName: "Wikidata",
            logo: "wikidata.svg",
            add: wikidataUI,
        },
        {
            fullName: "The Movie Database",
            shortName: "The Movie Database",
            logo: "themoviedb.svg",
            add: theMovieDBUI,
        },
        {
            fullName: "Discogs",
            shortName: "Discogs",
            logo: "discogs_vinyl_record_mark.svg",
            add: discogsUI,
        },
        {
            fullName: "World Health Organization (WHO)",
            shortName: "WHO",
            logo: "#008dc9",
            add: whoUI,
        },
        {
            fullName: "NASA Goddard Institue for Space Studies (GISS)",
            shortName: "NASA GISS",
            logo: "#0b3d91",
            add: nasaGoddardUI,
        },
        {
            fullName: "National Centers for Environmental Information (NCEI)",
            shortName: "NCEI",
            logo: "#162e51",
            add: NOAACDOUI,
        },
    ];

    function updateCalendars(ui) {
        for (var s = 0; s < ui.strips.length; s++) {
            var lowerCal = null, upperCal = null;
            if (s == 0) {
                upperCal = ui.calendars[1].instance;
            }
            if (s == ui.strips.length - 1) {
                lowerCal = ui.calendars[0].instance;
            }
            ui.strips[s].timeline.setCalendar(0, lowerCal);
            ui.strips[s].timeline.setCalendar(1, upperCal);
        }
    }

    function createCalendarSelector(element, ui, axis, label, initial) {
        var sel = element.append("label").html(label).append("select");
        sel.selectAll("option").data(CALENDARS).enter().append("option")
            .attr("value", c => c.id)
            .attr("selected", c => ((c.id == initial) ? "" : null))
            .html(c => c.name);
        sel.on("change", function() {
            var cal = CALENDARS[CALENDARS.findIndex(c => c.id == d3.event.target.value)];
            ui.calendars[axis] = cal;
            updateCalendars(ui);
        });
        sel.dispatch("change");

        var initialCal = CALENDARS[CALENDARS.findIndex(c => c.id == initial)];
    }

    function updateStripSelectors(ui) {
        for (let obj of ui.dataObjects) {
            var data = obj.stripSelect.selectAll("option").data(ui.strips.concat({}));
            data.enter().append("option")
                .merge(data)
                .attr("value", (s, i) => (i))
                .html((s, i) => ("timeline" in s ? "Strip " + (i + 1).toString() : ("(add new)")));
            data.exit().remove();
        }
    }

    function addStrip(ui) {
        var stripNum = ui.strips.length;
        var container = ui.timelineArea.append("div")
            .attr("id", "timeline-strip" + stripNum.toString());
        var timeline = new aeon.Timeline(container);
        timeline.setAutoAdjustHeight(true);
        timeline.setMaxTimeRange(null, ui.endTime);
        timeline.setMaxZoomFactor(1/360);

        if (ui.baseTimeline !== null) {
            ui.baseTimeline.connect(timeline);
        }

        var configRow = [];
        configRow.push(ui.stripConfigArea.append("span")
                       .attr("class", "strip-header")
                       .html("Strip " + (stripNum + 1).toString() + ":"));
        var strip = {
            container: container,
            configRow: configRow,
            timeline: timeline,
            datasets: [],
            yManagers:  [
                new yAxisManager(ui.stripConfigArea, configRow, timeline, 0, "Left Y axis"),
                new yAxisManager(ui.stripConfigArea, configRow, timeline, 1, "Right Y axis"),
            ],
        };

        var ehLabel = ui.stripConfigArea.append("label").html("Entity height:");
        var ehSel = ehLabel.append("select")
            .on("change", function() {
                timeline.setEntityHeight(parseInt(d3.event.target.value, 10));
            });
        ehSel.selectAll("option").data(ENTITY_HEIGHTS).enter().append("option")
            .attr("selected", d => (d.name == DEFAULT_ENTITY_HEIGHT ? "" : null))
            .attr("value", d => d.height)
            .html(d => d.name);
        ehSel.dispatch("change");
        strip.configRow.push(ehLabel);

        ui.strips.push(strip);

        updateYAxes(strip, null);
        updateCalendars(ui);
        updateStripSelectors(ui);

        return strip;
    }

    function removeStrip(ui) {
        ui.strips[ui.strips.length - 1].container.remove();
        for (let elem of ui.strips[ui.strips.length - 1].configRow) {
            elem.remove();
        }
        ui.strips.pop();

        updateCalendars(ui);
        updateStripSelectors(ui);
    }

    function yAxisManager(element, row, timeline, axis, label) {
        var self = this;
        this.kind = "none";
        var kindLabel = element.append("label").html(label + " quantity:");
        var unitLabel = element.append("label").html("Unit:")
        row.push(kindLabel, unitLabel);
        this.kindSel = kindLabel.append("select").attr("class", "kind-select");
        this.unitSel = unitLabel.append("select").attr("class", "unit-select").attr("disabled", "");
        this.kindSel.on("change", function() {
            self.kind = d3.event.target.value;
            var unitData = self.unitSel.selectAll("option").data(self.kind == "none" ? [] : units[self.kind].units);
            unitData.enter().append("option")
                .merge(unitData)
                .attr("value", (u, i) => i)
                .html(u => u.name);
            unitData.exit().remove();
            self.unitSel.attr("disabled", (self.kind == "none") ? "" : null);
            self.unitSel.dispatch("change");
        });
        this.unitSel.on("change", function() {
            if (self.kind == "none") {
                timeline.setYAxis(axis, null, null);
            } else {
                var unitInfo = units[self.kind].units[parseInt(d3.event.target.value)];
                var labelText = units[self.kind].name;
                var abbr = ("abbr" in unitInfo) ? unitInfo.abbr : unitInfo.name;
                if  (abbr != "") {
                    labelText += " (" + abbr + ")";
                }
                timeline.setYAxis(axis, self.kind, unitInfo.spec);
                timeline.setYLabel(axis, labelText);
            }
        });
    }

    yAxisManager.prototype.update = function(datasets) {
        var kinds = Array.from(new Set(datasets.filter(d => ("kind" in d)).map(d => d.kind)));
        kinds.unshift("none");
        var kindData = this.kindSel.selectAll("option").data(kinds);
        kindData.enter().append("option")
            .merge(kindData)
            .attr("value", k => k)
            //.attr("selected", c => ((c.id == initial) ? "" : null))
            .html(k => (k == "none" ? "None" : units[k].name));
        kindData.exit().remove();
        this.kindSel.dispatch("change");
    };

    yAxisManager.prototype.getKind = function() {
        return this.kind;
    };

    yAxisManager.prototype.setKind = function(kind, unit) {
        var unitIndex = 0;
        this.kindSel.property("value", kind);
        this.kindSel.dispatch("change");
        if (unit) {
            unitIndex = units[kind].units.findIndex(u => (u.name == unit));
            if (unitIndex == -1) {
                unitIndex = 0;
            }
        }
        this.unitSel.property("value", unitIndex);
        this.unitSel.dispatch("change");
    };

    function updateYAxes(strip, newKind, suggestedUnit) {
        var kindShown = false;
        for (let ym of strip.yManagers) {
            var oldKind = ym.getKind();
            var dsCount = strip.datasets.filter(d => ("kind" in d && d.kind == newKind)).length;
            ym.update(strip.datasets);
            if (newKind !== null && ym.getKind() == newKind) {
                if (newKind != oldKind || dsCount == 1) {
                    ym.setKind(newKind, suggestedUnit);
                }
                kindShown = true;
            }
        }
        if (newKind !== null && !kindShown) {
            for (let ym of strip.yManagers) {
                if (ym.getKind() == "none") {
                    ym.setKind(newKind, suggestedUnit);
                    break;
                }
            }
        }
    }

    function autoZoom(ui) {
        if (!ui.autozoom) {
            return;
        }
        var min = null, max = null;
        for (let obj of ui.dataObjects) {
            if (obj.guestDataset === null) {
                continue;
            }
            for (let d of obj.guestDataset.data) {
                switch (obj.guestDataset.type) {
                case "num":
                    if (min === null || d.time < min) {
                        min = d.time;
                    }
                    if (max === null || d.time > max) {
                        max = d.time;
                    }
                    break;
                case "entity":
                    if (min === null || d.start < min) {
                        min = d.start;
                    }
                    if (max === null || d.end === null || d.end > max) {
                        max = d.end ?? ui.endTime;
                    }
                    break;
                }
            }
        }
        if (min !== null && max !== null) {
            var margin;
            if (max > min) {
                margin = TIME_MARGIN_FRACTION * (max - min);
            } else {
                margin = TIME_MARGIN_ABSOLUTE;
            }
            ui.baseTimeline.setTimeRange(min - margin, max + margin);
        }
    }

    function addLogo(e, logoSpec) {
        if (logoSpec === undefined) {
            logoSpec = "#000";
        }
        if (logoSpec[0] == "#") {
            var size = 100;
            e.append("svg")
                .attr("class", "logo")
                .attr("viewBox", "0 0 " + size + " " + size)
                .append("circle")
                .attr("cx", size / 2)
                .attr("cy", size / 2)
                .attr("r", size / 2 - 1)
                .attr("stroke", logoSpec)
                .attr("fill", logoSpec);
        } else {
            e.append("img")
                .attr("src", LOGO_PATH + "/" + logoSpec)
                .attr("class", "logo");
        }
    }

    function DatasetManager(ui, dataSource) {
        this.ui = ui;
        this.dataSource = dataSource;
        this.box = this.ui.settingsArea.append("div").attr("class", "settings-box");
        this.guestDataset = null;
        this.rootGroup = new QueryGroup();
        this.downloadingData = false;
        this.configReady = false;
        this.addButton = null;
        this.entityStyle = null;

        var self = this;

        this.pickStartingColor = function (type, colors) {
            freqs = colors.reduce((a, c) => (a[c]=0, a), {});
            for (let obj of self.ui.dataObjects) {
                if (obj.type == type && "style" in obj)
                    freqs[obj.style[0]]++;
            }
            for (let f = 0; true; f++) {
                for (let color of colors) {
                    if (freqs[color] <= f)
                        return color;
                }
            }
        };

        this.updateStyle = function() {
            switch (self.type) {
            case "entity":
                var secondaryColor;
                switch (self.style[0].charAt(0)) {
                case 'b':
                    self.strip.timeline.setDatasetClass(self.ds, "dataset-bright-color");
                    secondaryColor = ENTITY_SECONDARY_COLOR_BRIGHT;
                    break;
                case 'd':
                    self.strip.timeline.setDatasetClass(self.ds, "dataset-dark-color");
                    secondaryColor = ENTITY_SECONDARY_COLOR_DARK;
                    break;
                }
                self.strip.timeline.setDatasetColor(self.ds, self.style[0].substring(2), secondaryColor);
                if (self.entityStyle !== null) {
                    self.strip.timeline.setDatasetEntityStyle(self.ds, self.entityStyle);
                }
                break;
            case "num":
                self.strip.timeline.setDatasetColor(self.ds, self.style[0]);
                self.strip.timeline.setDatasetPlotStyle(self.ds, self.style[1], self.style[2]);
                break;
            }
        };

        self.updateAddButton = function() {
            if (!self.addButton) {
                return;
            }
            if (self.configReady && !self.downloadingData) {
                self.addButton.attr("disabled", null);
            } else {
                self.addButton.attr("disabled", "");
            }
        };

        this.ui.dataObjects.push(this);
        this.unready();
        dataSource.add(this);
    }

    DatasetManager.prototype.ready = function() {
        this.configReady = true;
        this.updateAddButton();
    }

    DatasetManager.prototype.unready = function() {
        this.configReady = false;
        this.updateAddButton();
    }

    DatasetManager.prototype.createHeaderRow = function() {
        var self = this;
        var icons, selection, updater;
        switch (self.type) {
        case "entity":
            icons = [entityColorStyles];
            selection = [this.pickStartingColor("entity", ENTITY_COLORS)];
            updater = function(elem, sel) {
                elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px")
                    .style("background-color", sel[0].substring(2));
            };
            break;
        case "num":
            icons = [plotColorStyles, plotPointStyles, plotLineStyles];
            selection = [this.pickStartingColor("num", PLOT_COLORS),
                         DEFAULT_PLOT_POINT_STYLE, DEFAULT_PLOT_LINE_STYLE];
            updater = function(elem, sel) {
                var color = sel[0];
                var pointStyle = plotPointStyles.entries.find(s => (s.id == sel[1]));
                var lineStyle = plotLineStyles.entries.find(s => (s.id == sel[2]));
                var svg = elem.append("div")
                    .style("display", "flex")
                    .style("align-items", "center")
                    .append("svg")
                    .attr("width", STYLE_ICON_WIDTH.toString() + "px")
                    .attr("height", STYLE_ICON_HEIGHT.toString() + "px");
                var g = svg.append("g")
                    .attr("stroke", color)
                    .attr("fill", color)
                    .attr("transform", "translate("
                          + ((STYLE_ICON_WIDTH - STYLE_ICON_POINT_SIZE) / 2).toString() + ","
                          + ((STYLE_ICON_HEIGHT - STYLE_ICON_POINT_SIZE) / 2).toString() + ")");
                pointStyle.createSymbol(g, STYLE_ICON_POINT_SIZE);
                if (lineStyle.pattern !== undefined) {
                    svg.append("line")
                        .attr("stroke", color)
                        .attr("x1", 0)
                        .attr("x2", STYLE_ICON_WIDTH)
                        .attr("y1", STYLE_ICON_HEIGHT / 2)
                        .attr("y2", STYLE_ICON_HEIGHT / 2)
                        .attr("stroke-width", 1)
                        .attr("stroke-dasharray", (lineStyle.pattern === null) ? "none" : lineStyle.pattern.join(','));
                }
            };
            break;
        }
        var styleSelect = new widgets.iconSelect(icons);
        styleSelect.on("change", function(sel) {
            self.style = sel;
            if (self.added) {
                self.updateStyle();
            }
        });
        styleSelect.setPreviewUpdater(updater);
        styleSelect.setAlign("right");
        styleSelect.setSelection(selection);
        self.style = styleSelect.getSelection();
        styleSelect.root.style("display", "inline-grid");

        var heading = self.box.append("h1");
        addLogo(heading, self.dataSource.logo);
        heading.append("span")
            .html(self.dataSource.shortName);
        self.stripSelect = heading.append("select")
            .on("change", function() {
                if (self.added) {
                    var s = parseInt(d3.event.target.value, 10);
                    self.strip.timeline.removeDataset(self.ds);
                    self.strip.datasets.splice(self.strip.datasets.indexOf(self.guestDataset), 1);
                    updateYAxes(self.strip, null);
                    if (s >= self.ui.strips.length) {
                        self.strip = addStrip(self.ui);
                    } else {
                        self.strip = self.ui.strips[s];
                    }
                    self.strip.timeline.addDataset(self.guestDataset);
                    self.strip.datasets.push(self.guestDataset);
                    self.updateStyle();
                    updateYAxes(self.strip, self.guestDataset.kind ?? null, self.guestDataset.suggestedUnit ?? null);
                    autoZoom(self.ui);
                }
            });
        updateStripSelectors(self.ui);
        heading.append(styleSelect.node());
        heading.append("button")
            .attr("class", "close-button")
            .html("&#xD7;")
            .on("click", function() {
                if (self.added) {
                    self.strip.timeline.removeDataset(self.ds);
                    if (self.guestDataset !== null) {
                        self.strip.datasets.splice(self.strip.datasets.indexOf(self.guestDataset), 1);
                    }
                    self.guestDataset = null;
                    updateYAxes(self.strip, null);
                }
                self.ui.dataObjects.splice(self.ui.dataObjects.indexOf(self), 1)
                self.box.remove();
                self.rootGroup.invalidate();
            });
    }

    DatasetManager.prototype.createFooterRow = function(submitFunc, dataFunc) {
        var self = this;
        var dsGroup = new QueryGroup();
        dsGroup.invalidatedBy(self.rootGroup);
        self.addButton = self.box.append("p").html("&nbsp;").append("button")
            .attr("class", "add-update-button")
            .on("click", function() {
                var btn = d3.select(d3.event.target);
                submitFunc(self);
                if (self.guestDataset !== null) {
                    self.strip.datasets.splice(self.strip.datasets.indexOf(self.guestDataset), 1);
                }
                self.downloadingData = true;
                self.updateAddButton();
                dsGroup.query(self.ds.fetch(), function(res) {
                    if (dataFunc !== undefined) {
                        dataFunc(res);
                    }
                    if (self.added) {
                        self.strip.timeline.updateDataset(res);
                    } else {
                        var s = parseInt(self.stripSelect.property("value"), 10);
                        if (s >= self.ui.strips.length) {
                            self.strip = addStrip(self.ui);
                        } else {
                            self.strip = self.ui.strips[s];
                        }
                        self.strip.timeline.addDataset(res);
                    }
                    self.updateStyle();
                    self.guestDataset = res;
                    self.strip.datasets.push(res);
                    updateYAxes(self.strip, res.kind ?? null, res.suggestedUnit ?? null);
                    autoZoom(self.ui);
                    self.added = true;
                    btn.html("Update");
                    self.downloadingData = false;
                    self.updateAddButton();
                });
            })
            .html("Add");
        self.updateAddButton();
    }

    DatasetManager.prototype.setEntityStyle = function(style) {
        this.entityStyle = style;
    }

    function init(id) {
        var root = d3.select("#" + id);
        var ui = {
            calendars: [CALENDARS[0], CALENDARS[0]],
            strips: [],
            dataObjects: [],
            baseTimeline: null,
            autozoom: true,
            gregorianCal: new aeon.GregorianCalendar(),
        };
        var endYear = (new Date()).getFullYear() + 1;
        ui.endTime = ui.gregorianCal.dateToTimestamp([endYear, 1, 1]),

        ui.adderArea = root.append("div").attr("id", "adder-area");
        ui.settingsArea = root.append("div").attr("id", "settings-area");;
        ui.stripConfigArea = root.append("div").attr("class", "strip-config-area");
        ui.calendarConfigArea = root.append("div").attr("class", "calendar-config-area");
        ui.timelineArea = root.append("div").attr("id", "timeline");
        ui.infoArea = root.append("div").attr("id", "info-area");

        ui.baseTimeline = addStrip(ui).timeline;

        ui.baseTimeline.setTimeRange(ui.gregorianCal.dateToTimestamp([INITIAL_START_YEAR, 1, 1]),
                                     ui.endTime);

        createCalendarSelector(ui.calendarConfigArea, ui, 1, "Upper calendar:", DEFAULT_UPPER_CALENDAR);
        createCalendarSelector(ui.calendarConfigArea, ui, 0, "Lower calendar:", DEFAULT_LOWER_CALENDAR);
        var autoZoomSpan = ui.calendarConfigArea.append("span");
        ui.autoZoomControl = autoZoomSpan.append("input")
            .attr("type", "checkbox")
            .attr("name", "autozoom").attr("id", "autozoom")
            .property("checked", ui.autozoom)
            .on("change", function(e) {
                ui.autozoom = this.checked;
                autoZoom(ui);
            });
        autoZoomSpan.append("label")
            .attr("for", "autozoom")
            .html("Auto-zoom");
        updateCalendars(ui);

        ui.infoBox = new aeon.InfoBox(ui.infoArea);
        ui.infoBox.connect(ui.baseTimeline);

        for (let src of dataSources) {
            var button = ui.adderArea.append("button")
                .attr("class", "add-data-source")
                .on("click", function() {
                    new DatasetManager(ui, src);
                });
            addLogo(button, src.logo);
            button.append("span").html(src.fullName);
        }
    }

    return {
        init: init,
    };
});
