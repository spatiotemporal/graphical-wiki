define(function() {

    const KINDS = {
        "count": {
            name: "Count",
            units: [
                { spec: "each", name: "pieces", abbr: "" },
                { spec: "dozen", name: "dozen" },
                { spec: "gross", name: "gross" },
                { spec: "keach", name: "thousands", abbr: "thousands" },
                { spec: "Meach", name: "millions", abbr: "millions" },
                { spec: "Geach", name: "billions", abbr: "billions" },
            ],
        },
        "ratio":  {
            name: "Ratio",
            units: [
                { spec: "1", name: "pure ratio", abbr: "" },
                { spec: "percent", name: "percent", abbr: "%" },
                { spec: "permille", name: "permille", abbr: "&permil;" },
                { spec: "ppm", name: "parts per million", abbr: "ppm" },
                { spec: "ppb", name: "parts per billion", abbr: "ppb" },
                { spec: "ppt", name: "parts per trillion", abbr: "ppt" },
                { spec: "ppq", name: "parts per quadrillion", abbr: "ppq" },
            ],
        },
        "angle": {
            name: "Angle",
            units: [
                { spec: "radian", name: "radians", abbr: "rad" },
                { spec: "degree", name: "degrees", abbr: "&deg;" },
                { spec: "gradian", name: "gradians", abbr: "grad" },
            ],
        },
        "length": {
            name: "Length",
            units: [
                { spec: "nm", name: "nanometers", abbr: "m" },
                { spec: "um", name: "micrometers", abbr: "&mu;m" },
                { spec: "mm", name: "millimeters", abbr: "mm" },
                { spec: "cm", name: "centimeters", abbr: "cm" },
                { spec: "dm", name: "decimeters", abbr: "dm" },
                { spec: "m", name: "meters", abbr: "m" },
                { spec: "km", name: "kilometers", abbr: "km" },
                { spec: "inch", name: "inches", abbr: "in." },
                { spec: "foot", name: "feet", abbr: "ft." },
                { spec: "yard", name: "yards", abbr: "yd." },
                { spec: "mile", name: "miles", abbr: "mi." },
                { spec: "angstrom", name: "&Aring;ngstr&ouml;m", abbr: "&Aring;" },
                { spec: "AU", name: "Astronomical units", abbr: "AU" },
                { spec: "light-year", name: "Light years", abbr: "ly" },
                { spec: "parsec", name: "Parsecs", abbr: "pc" },
            ],
        },
        "time": {
            name: "Time",
            units: [
                { spec: "ns", name: "nanoseconds", abbr: "ns" },
                { spec: "us", name: "microseconds", abbr: "&mu;s" },
                { spec: "ms", name: "milliseconds", abbr: "ms" },
                { spec: "s", name: "seconds", abbr: "s" },
                { spec: "min", name: "minutes", abbr: "min" },
                { spec: "hr", name: "hours", abbr: "hours" },
                { spec: "days", name: "days", abbr: "days" },
                { spec: "week", name: "weeks", abbr: "weeks" },
                { spec: "year", name: "years", abbr: "years" },
            ],
        },
        "speed": {
            name: "Speed",
            units: [
                { spec: "m/s", name: "m/s" },
                { spec: "km/h", name: "km/h" },
                { spec: "mph", name: "miles/h" },
                { spec: "knot", name: "knots" },
            ],
        },
        "temperature": {
            name: "Temperature",
            units: [
                { spec: "tempC", name: "Celcius", abbr: "&deg;C" },
                { spec: "tempF", name: "Fahrenheit", abbr: "&deg;F" },
                { spec: "tempK", name: "Kelvin", abbr: "K" },
                { spec: "tempR", name: "Rankine", abbr: "&deg;R" },
            ],
        },
        "temperature_difference": {
            name: "Temperature difference",
            units: [
                { spec: "degC", name: "Celcius", abbr: "&deg;C" },
                { spec: "degF", name: "Fahrenheit", abbr: "&deg;F" },
            ],
        },
        "degree_days": {
            name: "Degree days",
            units: [
                { spec: "degK*s", name: "Kelvin seconds", abbr: "Ks" },
                { spec: "degC*day", name: "Celcius degree days", abbr: "&deg;C days" },
                { spec: "degF*day", name: "Fahrenheit degree days", abbr: "&deg;F days" },
            ],
        },
        "volumetric_flux": {
            name: "Volumetric flux",
            units: [
                { spec: "cmpssqm", name: "m&sup3;/m&sup2;s" },
                { spec: "lpssqm", name: "l/m&sup2;s" },
                { spec: "mmpd", name: "mm/day" },
            ],
        },
        "pressure": {
            name: "Pressure",
            units: [
                { spec: "Pa", name: "Pascal", abbr: "Pa" },
                { spec: "kPa", name: "Kilopascal", abbr: "kPa" },
                { spec: "bar", name: "Bar", abbr: "bar" },
                { spec: "mbar", name: "Millibar", abbr: "mbar" },
                { spec: "atm", name: "Atmopshere (atm)", abbr: "atm" },
                { spec: "torr", name: "Torr", abbr: "torr" },
                { spec: "mmHg", name: "mm Hg", abbr: "mmHg" },
                { spec: "inHg", name: "inches Hg", abbr: "inHg" },
                { spec: "lb/in^2", name: "Pounds per square inch", abbr: "psi" },
            ],
        },
        "concentration": {
            name: "Concentration",
            units: [
                { spec: "mol/m^3", name: "mol/m&sup3;" },
                { spec: "mol/l", name: "mol/L" },
                { spec: "mmol/l", name: "mmol/L" },
            ],
        },
        "body_mass_index": {
            name: "Body mass index (BMI)",
            units: [
                { spec: "kg/m^2", name: "kg/m&sup2;" },
            ],
        },
    };

    return KINDS;
});
