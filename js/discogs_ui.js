define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'config/credentials'], function(d3, widgets, aeon, credentials) {

    const INTRO_TEXT = '<p>Displays releases (i.e. albums and singles) by given artist.'
          + ' Data provided by <a href="https://discogs.com" target="_blank">Discogs</a>.</p>'
          + '<p>This application uses Discogs\' API but is not affiliated with, sponsored or endorsed by Discogs.'
          + ' \'Discogs\' is a trademark of Zink Media, LLC.</p>';

    return function(obj) {
        obj.added = false;
        obj.artist = null;
        obj.type = "entity";
        obj.ds = new aeon.DiscogsDataset();
        obj.ds.setAuthKey(credentials.DiscogsKey, credentials.DiscogsSecret);

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);
        var form = obj.box.append("form");

        var artistSelect = new widgets.searchBar();
        artistSelect.setSubmitByEntryMode(true);
        artistSelect.setSearchDelay(100);
        artistSelect.on("search", function(searchString) {
            return obj.ds.searchArtists(searchString);
        });
        artistSelect.setFrozenValueMode(true);
        artistSelect.on("submit", function(p) {
            obj.artist = p.id;
            obj.ready();
        });
        form.append("label").html("Artist:");
        form.append(artistSelect.node());

        obj.createFooterRow(function(obj) {
            obj.ds.setArtist(obj.artist);
        });
    };
});
