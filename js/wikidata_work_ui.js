define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'js/query_group', 'js/wikidata_helpers'],
       function(d3, widgets, aeon, QueryGroup, helpers) {

    const WORK_TYPES = [
        { "id": "Q3305213", "name": "Paintings" },
        { "id": "Q2188189", "name": "Musical works" },
        { "id": "Q860861", "name": "Sculptures" },
        { "id": "Q1416517", "name": "Cartoons" },
        { "id": "Q11424", "name": "Films" },
        { "id": "Q7725634", "name": "Literary works" },
        { "id": "Q629206", "name": "Computer languages" },
        { "id": "Q7397", "name": "Software" },
    ];

    return function(obj, typeData) {
        var form = typeData.form;
        var filters = typeData.filters = new helpers.FilterSet();

        form.append("label").html("Type of work:");
        var workTypeSelect = form.append("select")
            .on("change", function() {
                var types = d3.event.target.value;
                filters.set("instance-of-subclass-of",
                            (types === "any")
                            ? WORK_TYPES.map(e => e.id.split(",")).flat()
                            : types.split(","));
            });
        workTypeSelect.append("option")
            .attr("value", "any")
            .html("(any type)");
        for (let type of WORK_TYPES) {
            workTypeSelect.append("option")
                .attr("value", type.id)
                .html(type.name);
        }
        workTypeSelect.dispatch("change");

        form.append("label").html("Country of origin:");
        helpers.countryControl(form, obj.ds, filters, "country-of-origin");

        var languageSelect = new widgets.searchBar();
        obj.ds.getLanguages().then(function(resp) {
            languageSelect.setEntries(resp);
            languageSelect.setFrozenValueMode(true);
            languageSelect.setSubmitByEntryMode(true);
            languageSelect.on("submit", function(e) {
                filters.set("language-of-work-or-name", e);
            });
        });
        form.append("label").html("Language of work or name:");
        form.append(languageSelect.node())

        var movementSelect = new widgets.searchBar();
        obj.ds.getMovements().then(function(resp) {
            movementSelect.setEntries(resp);
            movementSelect.setFrozenValueMode(true);
            movementSelect.setSubmitByEntryMode(true);
            movementSelect.on("submit", function(e) {
                filters.set("movement", e);
            });
        });
        form.append("label").html("Movement:");
        form.append(movementSelect.node())

        var genreSelect = new widgets.searchBar();
        obj.ds.getGenres().then(function(resp) {
            genreSelect.setEntries(resp);
            genreSelect.setFrozenValueMode(true);
            genreSelect.setSubmitByEntryMode(true);
            genreSelect.on("submit", function(e) {
                filters.set("genre", e);
            });
        });
        form.append("label").html("Genre or category:");
        form.append(genreSelect.node())

        var copyrightSelect = new widgets.searchBar();
        obj.ds.getCopyrightLicenses().then(function(resp) {
            copyrightSelect.setEntries(resp);
            copyrightSelect.setFrozenValueMode(true);
            copyrightSelect.setSubmitByEntryMode(true);
            copyrightSelect.on("submit", function(e) {
                filters.set("copyright-license", e);
            });
        });
        form.append("label").html("Copyright license:");
        form.append(copyrightSelect.node())

        var awardSelect = new widgets.searchBar();
        obj.ds.getAwards().then(function(resp) {
            awardSelect.setEntries(resp);
            awardSelect.setFrozenValueMode(true);
            awardSelect.setSubmitByEntryMode(true);
            awardSelect.on("submit", function(e) {
                filters.set("award", e);
            });
        });
        form.append("label").html("Award received:");
        form.append(awardSelect.node())
    }

});
