define(['d3', 'd3-widgets/src/index', 'aeon/aeon', 'config/credentials'], function(d3, widgets, aeon, credentials) {

    const MAX_PAGES = 3;
    const MAX_MOVIES = 20 * MAX_PAGES;
    const INTRO_TEXT = '<p>Displays movies from'
          + ' <a href="https://www.themoviedb.org/" target="_blank">The Movie Database</a> (TMDb).'
          + ' Filter on genre and any number of actors or crew members that all have to be included in the production.'
          + ' If more than ' + MAX_MOVIES + ' movies match your query the ' + MAX_MOVIES
          + ' most popular matches are shown.</p>'
          + '<p>This product uses the TMDb API but is not endorsed or certified by TMDb.</p>';

    return function(obj) {
        obj.added = false;
        obj.people = new Set();
        obj.type = "entity";
        obj.ds = new aeon.TheMovieDBDataset();
        obj.ds.setAccessToken(credentials.TheMovieDBAccessToken);
        obj.ds.setMaxPages(MAX_PAGES);

        obj.createHeaderRow();

        obj.box.append("div").attr("class", "data-source-intro").html(INTRO_TEXT);
        var form = obj.box.append("form");

        var genresSelect = new widgets.multiSelect();
        genresSelect.allShortcut(true, true);
        form.append("label").html("Genres:");
        form.append(genresSelect.node());

        var peopleSelect = new widgets.searchBar();
        peopleSelect.setSubmitByEntryMode(true);
        peopleSelect.setSearchDelay(100);
        peopleSelect.on("search", function(searchString) {
            return obj.ds.searchPerson(searchString);
        });
        peopleSelect.on("submit", function(p) {
            if (obj.people.has(p.id)) {
                return;
            }
            obj.people.add(p.id);
            var pBox = peopleList.append("span")
                .attr("class", "value-box");
            pBox.append("span").html(p.name);
            pBox.append("span")
                .attr("class", "value-box-close-icon")
                .html("&#xD7;")
                .on("click", function() {
                    obj.people.delete(p.id);
                    d3.event.target.parentElement.remove();
                });
        });
        form.append("label").html("Actors or crew:");
        form.append(peopleSelect.node());
        form.append("span");
        var peopleList = form.append("span")
            .style("display", "flex")
            .style("flex-wrap", "wrap")
            .style("gap", "8px");

        obj.ds.getGenres().then(function(resp) {
            for (let genre of resp) {
                genre.selected = true;
            }
            genresSelect.setEntries(resp);
            obj.ready();
        });

        obj.createFooterRow(function(obj) {
            var genres = genresSelect.getSelection();
            if (genres === "all") {
                obj.ds.setGenres(null);
            } else {
                obj.ds.setGenres(genres);
            }
            if (obj.people.size == 0) {
                obj.ds.setPeople(null);
            } else {
                obj.ds.setPeople(Array.from(obj.people));
            }
        });
    };
});
